#!/bin/bash
# To set Firefox to open external links in the same (active) tab/window, set browser.link.open_newwindow.override.external = 1 (default is -1)
# See https://support.mozilla.org/en-US/questions/959136
# Turn off your screensaver before running

# What is the current "Start after" setting for the screensaver?
screensavertime=$(defaults -currentHost read com.apple.screensaver idleTime)
# Turn off screensaver
defaults -currentHost write com.apple.screensaver idleTime 0
today=$(date +%Y%m%d)
mkdir -p $today
while read url
do
  # Baby, what time is it?
  timestamp=$(date +%Y%m%d%H%M%S)
  url=$(echo $url | tr -d "\r")
  name=$(echo $url | sed 's/https:\/\///g' | sed 's/people\.stanford\.edu\//people-/g' | sed 's/sites\.stanford\.edu\//sites-/g' | sed 's/\///g')
  open -g $url -a "Firefox"
#  sleep 10
  screencapture -x -D 2 -T 10 $today/$name-$timestamp.jpg

done < sites-list.txt
# Return screensaver to the previous setting
defaults -currentHost write com.apple.screensaver idleTime $screensavertime
