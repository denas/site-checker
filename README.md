# About

# Setup

## screencapture.sh Script

Open System Preferences. Go to "Security and Privacy", then click the "Privacy"
tab. Choose "Screen Recording", and check the box for "Terminal".

![Screenshot of System Preferences](SystemPreferences.jpg)

# Usage

## site-checker Script
```
site-checker -i input-file.csv -o output-file.csv "Text to search for"
```

See `example-input.csv` and `example-output.csv` for the format of files.

For instance, using "Once you have logged in, you will be able to remove this
text from your homepage" as the "Text to search for" argument will return:

```
jbickar,https://jbickar.stanford.edu/,yes
```

if the text exists on the site (thus indicating that it likely **is not** in
use)

and

```
jbickar,https://jbickar.stanford.edu/,no
```

if the text does not exist on the homepage of the site (thus indicating that it
  likely **is** in use)

## screencapture.sh Script

1. Set Firefox to open external links in the same (active) tab/window:
    1. Type `about:config` in the address bar and hit `<Return>`
    2. Type `browser.link.open_newwindow.override.external`
    3. Double-click the `-1` (the default) and set it to `1`
2. Turn your screensaver off (the script **should** do this but it's sometimes
unreliable).
3. Create a text file with one URL on each line and save it as `sites-list.txt`
(see `example-sites-list.txt` for a template)
4. `./screencapture.sh`

There are currently no arguments. It is set to capture on the second display, so
if you want to to capture on the main display, modify the `-D` parameter in the
`screencapture.sh`, and create a pull request to make that dynamic.

Remember to set `browser.link.open_newwindow.override.external` back to `-1`
when you are finished running the script, unless you like having new links open
in the same window.

# Troubleshooting

## site-checker script

Error:

```
<urlopen error [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed:
Hostname mismatch, certificate is not valid for 'ehalilaj.people.stanford.edu'.
(_ssl.c:1123)> ERROR: SSL problems. Does this machine have an updated "certifi"
Python library?
```

Solution:

```
pip3 install certifi --upgrade
```

You may still encounter that error after upgrading the `certifi` library. If you
do, check the URL in a browser and accept the invalid certificate. If you get
the message, "Web Site Not Found. Sorry, we could not find any content for this
web address. Please check the URL.", that means that the site does not exist on
Acquia.
